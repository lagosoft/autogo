package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"strings"

	"github.com/gorilla/mux"
)

//Object to represent the resulting file from
//IFTTT Recipe used craigslist->dropbox
type IRecipeResult struct {
	Filename string
	Url      string
	Notes    string
	Path     string
}

const (
	//To run localy replace the folder to exact folder path, since the plan is to
	//run from docker we will pass in the folder path via:
	// Dockerfile or docker-compose or docker run command
	IRecipeFolder = "./IFTTT/Craigslist/"
)

var tpl *template.Template
var AllResults []IRecipeResult

func main() {
	gorillaRoute := mux.NewRouter()
	gorillaRoute.HandleFunc("/", getTsxResults).Name("home")
	gorillaRoute.HandleFunc("/delete/{filename}", handleDeleteResult)
	gorillaRoute.HandleFunc("/mail", sendEmail).Methods("POST")
	http.HandleFunc("/css/", serverCSS)
	http.Handle("/", &WithCORS{gorillaRoute})
	log.Fatal(http.ListenAndServe(":8080", nil))
}

type WithCORS struct {
	r *mux.Router
}

// Simple wrapper to Allow CORS.
// See: http://stackoverflow.com/a/24818638/1058612.
func (s *WithCORS) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if origin := req.Header.Get("Origin"); origin != "" {
		res.Header().Set("Access-Control-Allow-Origin", origin)
		res.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		res.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}
	// Stop here for a Preflighted OPTIONS request.
	if req.Method == "OPTIONS" {
		return
	}
	// Lets Gorilla work
	s.r.ServeHTTP(res, req)
}

func sendEmail(w http.ResponseWriter, r *http.Request) {
	//Get the bytes of the request body and into a string
	buf := bytes.NewBuffer(make([]byte, 0, r.ContentLength))
	_, err := buf.ReadFrom(r.Body)
	body := buf.Bytes()
	fmt.Println("REQ BODY: ", string(body))
	if err != nil {
		fmt.Println("got error : ", err)
		panic(err)
	}

	//Take the JSON of the body and put it in a struct.
	decoder := json.NewDecoder(buf)
	var contactMessage struct {
		Name  string
		Email string
		Url   string
		Note  string
	}
	err = decoder.Decode(&contactMessage)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	fmt.Println(contactMessage.Name, contactMessage.Email, contactMessage.Url, contactMessage.Note)

	// Set up authentication information.
	auth := smtp.PlainAuth(
		"",
		"cruz.fabio27@gmail.com",
		"<PWD>",
		"smtp.gmail.com",
	)

	msg := "From: cruz.fabio27@gmail.com\n" +
		"To: lagosoundsoftware@gmail.com\n" +
		"Subject: fabiovalentino.com Contact Message\n" +

		"'" + contactMessage.Note + "''\n\n" +
		"From : " + contactMessage.Name + "\n" +
		"Email : " + contactMessage.Email + "\n" +
		"Url : " + contactMessage.Url + "\n\n" +
		"-=[ E O M ]=-"

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	err = smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"contact.form@fabiovalentino.com",
		[]string{"lagosoundsoftware@gmail.com"},
		[]byte(msg),
	)
	if err != nil {
		log.Fatal(err)
	} else {
		w.Write([]byte(string("mail sent succesfully!")))
	}
}

func init() {
	//prepare function map for template
	// fmap := template.FuncMap {
	// 	"deleteFile" : deleteFile }
	// tpl = template.Must(template.New("").Funcs(fmap).ParseFiles("tsx.template.gohtml"))
	tpl = template.Must(template.ParseFiles("tsx.template.gohtml"))
	fmt.Println("templates parsed ok")
	processRecipeFiles()
}

func getTsxResults(w http.ResponseWriter, r *http.Request) {
	// http.FileServer(http.Dir("./"))
	// w.Write([]byte(" CONTENTS OF SOME DOPE PAGE!"))
	htmlFile, err := os.Create("index.html")
	check(err)
	defer htmlFile.Close()

	err2 := tpl.Execute(htmlFile, AllResults[:len(AllResults)])
	if err2 != nil {
		fmt.Println(err2)
	}

	dat, err := ioutil.ReadFile("index.html")
	check(err)
	w.Write([]byte(string(dat)))
}

func handleDeleteResult(w http.ResponseWriter, r *http.Request) {
	urlParams := mux.Vars(r)
	filename := urlParams["filename"]
	var fileToDelete = ""
	for i := 0; i < len(AllResults); i++ {
		if AllResults[i].Filename == filename {
			fileToDelete = filename
			AllResults = append(AllResults[:i], AllResults[i+1:]...)
			break
		}
	}

	if fileToDelete != "" {
		fmt.Println("Deleting: ", filename)
		deleteFile(IRecipeFolder + fileToDelete)
		//and redirect to home after the delete.
		http.Redirect(w, r, "http://localhost:8080", 302)
	} else {
		w.Write([]byte(" deleted 0 files"))
	}
}

func serverCSS(w http.ResponseWriter, req *http.Request) {
	path := "." + req.URL.Path
	var contentType string
	if strings.HasSuffix(path, ".css") {
		contentType = "text/css; charset=utf-8"
	} else {
		contentType = "text/plain; charset=utf-8"
	}
	f, err := os.Open(path)
	if err == nil {
		defer f.Close()
		w.Header().Add("Content-Type", contentType)
		br := bufio.NewReader(f)
		br.WriteTo(w)
	} else {
		w.WriteHeader(404)
	}
}

func processRecipeFiles() {
	//list all files in a directory
	files, _ := ioutil.ReadDir(IRecipeFolder)
	AllResults = make([]IRecipeResult, len(files))
	i := 0
	for _, f := range files {
		if f.Name() == ".DS_Store" {
			continue
		}
		filename := IRecipeFolder + f.Name()
		dat, err := ioutil.ReadFile(filename)
		check(err)

		url := getAdsURL(string(dat))
		validAndCurrent := isValidUrl(string(url))
		fmt.Println(url, validAndCurrent)
		if validAndCurrent {
			result := IRecipeResult{
				Filename: f.Name(),
				Url:      url,
				Notes:    string(dat),
				Path:     filename}

			//add result to (slice) allResults
			AllResults[i] = result
			i += 1
		} else {
			deleteFile(filename)
		}
	}
	AllResults = AllResults[:i]
	fmt.Println("total results ", i)
}

func deleteFile(filename string) string {
	err := os.Remove(filename)

	if err != nil {
		fmt.Println(err)
		return "error"
	}
	return "deleted"
}

func getAdsURL(contents string) string {
	beginIndex := strings.Index(contents, "http://ift.tt")
	if beginIndex < 0 {
		beginIndex = strings.Index(contents, "http://")
	}
	url := contents[beginIndex:len(contents)]
	splitByNewLine := strings.Split(url, "\n")[0]
	return strings.Split(strings.TrimSpace(splitByNewLine), " ")[0] //split by space
}

func isValidUrl(url string) bool {
	if len(url) > 0 {
		contents := getPageContents(url)
		return isActiveOnCraigslist(contents)
	} else {
		return false
	}
}

func isActiveOnCraigslist(contents string) bool {
	active := strings.Index(contents, "www.craigslist.org") > 0 && strings.Index(contents, "posting has expired") == -1
	notRecentlyRemoved := strings.Index(contents, "(The title on the listings page will be removed in just a few minutes.)") == -1
	notRemovedByAuthor := strings.Index(contents, "has been deleted by its author") == -1
	return active && notRecentlyRemoved && notRemovedByAuthor
}

func getPageContents(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		return "ERROR!"
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	pageContents := string(body)
	return pageContents
}

func printResultFilenames() {
	for i := 0; i < len(AllResults); i++ {
		fmt.Println("index : ", i, AllResults[i].Filename)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
