FROM golang:1.6.1-alpine

RUN apk update && apk upgrade && apk add git

RUN go get -u github.com/gorilla/mux

WORKDIR /go/src/fvc/automation/

EXPOSE 8080